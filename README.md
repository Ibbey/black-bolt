# BlackBolt

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng local` for a localized development server with mock data. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build:dev` to build the project in development mode.  The build artifacts will be stored in the `dist/dev` directory.

Run `ng build:production` to build the project in production mode. The build artifacts will be stored in the `dist/production` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
