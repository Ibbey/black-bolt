### Stage 1 - Build the Black-Bolt Application
### Can change the build target with the TARGET build argument

FROM node:latest AS BUILDER
LABEL Author="Ryan Barriger"

COPY package.json package.json

### Force an install of node-sass - AOT will complain/reject later on
RUN npm install && npm install node-sass@latest && mkdir /permutr-app && mv ./node_modules ./permutr-app

WORKDIR /black-bolt-app

COPY . .
RUN $(npm bin)/npm run lint

RUN $(npm bin)/npm run test

RUN $(npm bin)/npm run build:dev

### Stage 2 - Setup and copy the built Permutr Application to the nginx image

FROM nginx:alpine

COPY nginx/nginx.dev.conf /etc/nginx/nginx.conf

### Remove any lingering files or default files that were installed by the base image
RUN rm -rf /usr/share/nginx/html/*

COPY --from=BUILDER /black-bolt-app/dist/dev /usr/share/nginx/html

### Kick the nginx proxy
CMD [ "nginx", "-g", "daemon off; "]
