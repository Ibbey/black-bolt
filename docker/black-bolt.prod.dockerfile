FROM nginx:alpine

COPY nginx/nginx.prod.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY dist/dev /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off; " ]
