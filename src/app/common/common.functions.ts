export function coerceBoolean(value: any): boolean {
    if (typeof(value) === 'boolean') {
        return value;
    }
    return !!value;
}

export function coerceNumber(value: any): number {
    if (typeof(value) === 'number') {
        return value;
    }
    return Number(value);
}

export function dateEquals(date1: Date, date2: Date): boolean {
    if (date1 === undefined || date2 === undefined) {
        return false;
    }

    if (date1.getFullYear() === date2.getFullYear()) {
        if (date1.getMonth() === date2.getMonth()) {
            if (date1.getDate() === date2.getDate()) {
                return true;
            }
        }
    }

    return false;
}

export function pureCollectionAdd(item: any, source: any[]): any[] {
    if (source === undefined) {
        return [ item ];
    }
    if (item !== undefined && source !== undefined) {
        const clone = source.slice(0);
        clone.push(item);

        return clone;
    }

    return undefined;
}
