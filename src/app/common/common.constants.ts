export const CELL_EDIT_NONE = 'none';
export const CELL_EDIT_STRING = 'string';
export const CELL_EDIT_NUMBER = 'number';
export const CELL_EDIT_DATE = 'date';
export const CELL_EDIT_CLOSED_DROPDOWN = 'dropdown/restricted';
export const CELL_EDIT_OPEN_DROPDOWN = 'dropdown/unrestricted';
