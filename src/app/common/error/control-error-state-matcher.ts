import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class ControlErrorStateMatcher implements ErrorStateMatcher {

    // Interface Methods **************************************************************************
    public isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {

        const isSubmitted = form && form.submitted;

        if (isSubmitted === false) {
            if (control !== undefined && control.invalid === false) {
                if (control.dirty === true && control.touched === true) {
                    return true;
                }
            }
        }

        return false;
    }
}
