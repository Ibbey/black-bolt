import { IConfigurationColumn, IConfigurationError } from '@models/configuration/_configuration.barrel';

import { CELL_EDIT_NONE, CELL_EDIT_STRING, CELL_EDIT_DATE, CELL_EDIT_NUMBER } from '@common/common.constants';

export class IGXColumnDefinition {
    // Member Variables ***************************************************************************
    private readonly _source: IConfigurationColumn;
    private readonly _isError: boolean;
    private readonly _isMassUpdate: boolean;

    private _header: string;
    private _field: string;
    private _dataType: string;
    private _sortable: boolean;
    private _resizable: boolean;
    private _editable: boolean;
    private _width: string;

    // Public Properties **************************************************************************
    public get source(): IConfigurationColumn {
        return this._source;
    }

    public get header(): string {
        return this._header;
    }

    public get field(): string {
        return this._field;
    }

    public get dataType(): string {
        return this._dataType;
    }

    public get sortable(): boolean {
        return this._sortable;
    }

    public get resizable(): boolean {
        return this._resizable;
    }

    public get editable(): boolean {
        return this._editable;
    }

    public get width(): string {
        return this._width;
    }

    public set width(value: string) {
        this._width = value;
    }

    public get canMassUpdate(): boolean {
        if (this._source !== undefined) {
            return this._source.massUpdate ? this._source.massUpdate : false;
        }

        return false;
    }

    public get options(): string[] {
        if (this._source && this._source.options) {
            return this._source.options;
        }

        return undefined;
    }

    public get isErrorColumn(): boolean {
        return this._isError;
    }

    public get isMassUpdateColumn(): boolean {
        return this._isMassUpdate;
    }

    public get editMode(): string {
        if (this._editable === true && this._source !== undefined) {
            return this.calculateEditMode();
        }

        return CELL_EDIT_NONE;
    }

    // Constructor ********************************************************************************
    constructor (source: IConfigurationColumn, isError: boolean = false, isMassUpdate: boolean = false) {
        this._source = source;
        this._isError = isError;
        this._isMassUpdate = isMassUpdate;

        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        if (this._source !== undefined) {
            this._dataType = this._source.dataType;
            this._editable = this._source.editable === undefined ? false : this._source.editable;
            this._field = this._source.key;
            this._header = this._source.displayName;
            this._resizable = true;
            this._sortable = true;
        }

        if (this._isError === true) {
            this._header = ' ';
            this._width = '5%';
            this._field = 'errors';
        }

        if (this._isMassUpdate === true) {
            this._header = ' ';
            this._width = '5%';
            this._field = 'selected';
        }
    }

    private calculateEditMode(): string {
        if (this._source !== undefined) {
            if (this._source.editType === 'text') {
                if (this._source.dataType === 'number') {
                    return CELL_EDIT_NUMBER;
                }

                if (this._source.dataType === 'date') {
                    return CELL_EDIT_DATE;
                }

                return CELL_EDIT_STRING;
            }

            return this._source.editType;
        }
    }
}
