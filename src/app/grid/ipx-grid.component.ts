import { Component, Input, Output, EventEmitter, AfterContentInit, ViewChild } from '@angular/core';

import { IgxGridComponent, IgxColumnComponent, IGridCellEventArgs, IgxDropDownComponent, OverlaySettings,
         IgxInputGroupComponent, NoOpScrollStrategy, ConnectedPositioningStrategy, IgxGridCellComponent,
         IGridEditEventArgs } from 'igniteui-angular';

import { IConfigurationEntry, IConfigurationError, IConfigurationDropDownMapping,
         IConfigurationAffectedColumn, IConfigurationModification} from '@models/configuration/_configuration.barrel';
import { IGXColumnDefinition } from './models/column-definition';

import { CELL_EDIT_CLOSED_DROPDOWN, CELL_EDIT_OPEN_DROPDOWN } from '@common/common.constants';
import { coerceBoolean, coerceNumber } from '@common/common.functions';

const WARNING_LEVEL = 'WARN';
const ERROR_LEVEL = 'ERROR';

interface IRowMapping {
    index: number;
    mappings: IConfigurationDropDownMapping[];
}

@Component({
    selector: 'app-black-bolt-grid',
    templateUrl: './ipx-grid.component.html',
    styleUrls: [ './ipx-grid.component.scss' ]
})
export class BlackBoltGridComponent implements AfterContentInit {
    // Member Variables ***************************************************************************
    private _itemsSource: IConfigurationEntry;
    private _definitions: IGXColumnDefinition[];
    private _data: any[];
    private _rowMappings: IRowMapping[];

    private _overlaySettings: OverlaySettings;

    private _cellStyleClasses: any;

    @ViewChild('grid') private _grid: IgxGridComponent;
    @ViewChild('dropdownTarget', { read: IgxInputGroupComponent }) private _dropDownTarget: IgxInputGroupComponent;
    @ViewChild('dropdown', { read: IgxDropDownComponent }) private _dropDown: IgxDropDownComponent;

    @Output() modified: EventEmitter<IConfigurationModification> = new EventEmitter<IConfigurationModification>();

    // Public Properties **************************************************************************
    @Input() public get itemsSource(): IConfigurationEntry {
        return this._itemsSource;
    }

    public set itemsSource(value: IConfigurationEntry) {
        this._itemsSource = value;
        if (this._itemsSource !== undefined) {
            this.load();
        }
    }

    @Input() public get definitions(): IGXColumnDefinition[] {
        return this._definitions;
    }

    public set definitions(value: IGXColumnDefinition[]) {
        this._definitions = value;
    }

    @Input() public get data(): any[] {
        return this._data;
    }

    public set data(value: any[]) {
        this._data = value;
    }

    public get cellStyleClasses(): any {
        return this._cellStyleClasses;
    }

    // Constructor ********************************************************************************
    constructor () {
        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngAfterContentInit(): void {
        if (this._grid !== undefined) {

        }
    }

    // Public Methods *****************************************************************************
    public toggleDropDown(): void {
        this._overlaySettings.positionStrategy.settings.target = this._dropDownTarget.element.nativeElement;
        this._dropDown.toggle(this._overlaySettings);
    }

    public isCellError(cell: IgxGridCellComponent, content: any): boolean {
        if (cell === undefined || cell.cellID === undefined) {
            return false;
        }

        const def = this.getColumnDefinition(cell.cellID.columnID);
        if (def !== undefined) {
            const rowData = cell.row.rowData;
            if (rowData !== undefined) {
                if (rowData['errors'] !== undefined && rowData['errors'].length !== 0) {
                    return this.getErrorLevel(rowData, def.field) !== undefined;
                }
            }
        }

        return false;
    }

    public getErrorMessage(cell: IgxGridCellComponent, content: any): string {
        if (cell === undefined || cell.cellID === undefined) {
            return undefined;
        }

        const def = this.getColumnDefinition(cell.cellID.columnID);
        if (def !== undefined) {
            const rowData = cell.row.rowData;
            if (rowData !== undefined) {
                const errors: IConfigurationError[] = rowData['errors'];
                if (errors !== undefined) {
                    const entry: IConfigurationError[] = errors.filter(x => x.columnKey === def.field);
                    if (entry !== undefined && entry.length === 1) {
                        return entry[0].message;
                    }
                }
            }
        }

        return undefined;
    }

    public refresh(content: any): void {
        this.itemsSource = content;
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._overlaySettings = {
            closeOnOutsideClick: true,
            modal: false,
            positionStrategy: new ConnectedPositioningStrategy(),
            scrollStrategy: new NoOpScrollStrategy()
        };

        this._cellStyleClasses = {
            levelWarning: this.warningCondition.bind(this),
            levelError: this.errorCondition.bind(this)
        };
    }

    private load(): void {
        this.extractDefinitions();
        this.extractData();
        this.extractRowMappings();
    }

    private extractDefinitions(): void {
        this._definitions = undefined;

        const list: IGXColumnDefinition[] = [];

        list.push(this.getErrorColumn());
        list.push(this.getMassUpdateColumn());

        if (this._itemsSource !== undefined && this._itemsSource.columns !== undefined) {
            this._itemsSource.columns.forEach(x => {
                const definition = new IGXColumnDefinition(x);
                list.push(definition);
            });
        }

        if (list.length !== 0) {
            this._definitions = list;
        }
    }

    private extractData(): void {
        this._data = undefined;

        const list: any[] = [];
        if (this._definitions !== undefined && this._itemsSource !== undefined && this._itemsSource.items !== undefined) {
            for (let j = 0; j < this._itemsSource.items.length; j++) {
                const item: any = {};
                item['errors'] = this._itemsSource.items[j].errors && this._itemsSource.items[j].errors.length > 0
                                     ? this._itemsSource.items[j].errors
                                     : undefined;
                item['selected'] = false;

                for (let i = 2; i < this._definitions.length; i++) {
                    const definition: IGXColumnDefinition = this._definitions[i];
                    if (definition.isErrorColumn === false && definition.isMassUpdateColumn === false) {
                        item[definition.field] = this.getValue(this._itemsSource.items[j].values[i - 2], definition);
                    }
                }
                list.push(item);
            }
        }

        if (list.length !== 0) {
            this.data = list;
        }
    }

    private extractRowMappings(): void {
        this._rowMappings = undefined;

        const list: IRowMapping[] = [];

        if (this._itemsSource !== undefined && this._itemsSource.items !== undefined) {
            for (let i = 0; i < this._itemsSource.items.length; i++) {
                const mappings: IConfigurationDropDownMapping[] = this._itemsSource.items[i].dropDownMapping;
                const item: IRowMapping = {
                    index: i,
                    mappings: mappings
                };

                list.push(item);
            }
        }

        if (list.length !== 0) {
            this._rowMappings = list;
        }
    }

    private getErrorColumn(): IGXColumnDefinition {
        return new IGXColumnDefinition(undefined, true, false);
    }

    private getMassUpdateColumn(): IGXColumnDefinition {
        return new IGXColumnDefinition(undefined, false, true);
    }

    private massUpdateValue(colIndex: number, rowIndex: number, newValue: any): void {
        if (this._data !== undefined) {
            for (let i = 0; i < this._data.length; i++) {
                if (i !== rowIndex) {
                    const row = this._data[i];
                    if (row['selected'] === true) {
                        const def = this.getColumnDefinition(colIndex);
                        if (def !== undefined) {
                            if (def.canMassUpdate === true) {
                                this._data[i][def.field].content = newValue;
                            }
                        }
                    }
                }
            }
        }
    }

    private updateAffectedColumns(def: IGXColumnDefinition, rowIndex: number, newValue: any): void {
        if (this._data !== undefined) {
            const row = this._data[rowIndex];
            if (row !== undefined) {
                const mappings: IConfigurationDropDownMapping[] = this.getRowMapping(rowIndex);
                if (mappings !== undefined) {
                    for (let i = 0; i < mappings.length; i++) {
                        if (mappings[i].columnKey === def.field) {
                            if (mappings[i].value === newValue) {
                                this.applyAffectedColumns(row, mappings[i].affectedColumns);
                            }
                        }
                    }
                }
            }
        }
    }

    private applyAffectedColumns(rowData: any, columns: IConfigurationAffectedColumn[]): void {
        if (rowData !== undefined && columns !== undefined) {
            columns.forEach((column: IConfigurationAffectedColumn) => {
                if (column !== undefined) {
                    const cell = rowData[column.columnKey];
                    if (cell !== undefined) {
                        cell['content'] = column.value;
                        cell['options'] = column.options;
                    }
                }
            });
        }
    }

    private  warningCondition(rowData: any, colField: string): boolean {
        if (rowData !== undefined) {
            const level = this.getErrorLevel(rowData, colField);
            return level === WARNING_LEVEL;
        }
    }

    private errorCondition(rowData: any, colField: string): boolean {
        if (rowData !== undefined) {
            const level = this.getErrorLevel(rowData, colField);
            return level === ERROR_LEVEL;
        }
    }

    private getRowMapping(index: number): IConfigurationDropDownMapping[] {
        if (this._rowMappings !== undefined) {
            for (let i = 0; i < this._rowMappings.length; i++) {
                if (this._rowMappings[i].index === index) {
                    return this._rowMappings[i].mappings;
                }
            }
        }

        return undefined;
    }

    private getErrorLevel(rowData: any, field: string): string {
        if (rowData !== undefined) {
            const errors: IConfigurationError[] = rowData['errors'];
            if (errors !== undefined && errors.length !== 0) {
                for (let i = 0; i < errors.length; i++) {
                    if (errors[i].columnKey === field) {
                        return errors[i].level;
                    }
                }
            }
        }

        return undefined;
    }

    private getColumnDefinition(index: number): IGXColumnDefinition {
        if (this._definitions !== undefined) {
            if (this._definitions.length > index) {
                return this._definitions[index];
            }
        }

        return undefined;
    }

    private getColumnIndex(cell: any): number {
        if (cell !== undefined) {
            return cell.columnIndex;
        }

        return -1;
    }

    private getRowIndex(cell: any): number {
        if (cell !== undefined) {
            return cell.rowIndex;
        }

        return -1;
    }

    private getValue(source: any, definition: IGXColumnDefinition): any {
        const value: any = {};
        value['content'] = this.coerceValue(source, definition.dataType);
        value['options'] = definition.editMode === CELL_EDIT_CLOSED_DROPDOWN || definition.editMode === CELL_EDIT_OPEN_DROPDOWN
                           ? definition.options
                           : undefined;

        return value;
    }

    private getModification(): IConfigurationModification {
        const table: any[] = [];

        if (this._data !== undefined) {
            for (let i = 0; i < this._data.length; i++) {
                const row = this._data[i];
                const rowData: any[] = [];
                for (let j = 2; j < this._definitions.length; j++) {
                    const def = this._definitions[j];
                    rowData[j - 2] = row[def.field].content;
                }

                table.push(rowData);
            }
        }

        return <IConfigurationModification>{
            rows: table
        };
    }

    private coerceValue(value: any, type: string): any {
        if (type === 'date') {
            return new Date(value);
        }
        if (type === 'number') {
            return coerceNumber(value);
        }
        if (type === 'boolean') {
            return coerceBoolean(value);
        }

        return value;
    }

    private notify(): void {
        const modification = this.getModification();
        this.modified.emit(modification);
    }

    // Event Handlers *****************************************************************************
    public handleOnColumnInit(event: IgxColumnComponent): void {

    }

    public handleOnSelection(event: IGridCellEventArgs): void {

    }

    public handleOnCellEdit(event: IGridEditEventArgs): void {
        if (event !== undefined) {
            const colIndex: number = event.cellID.columnID;
            const rowIndex: number = event.cellID.rowIndex;
            const def: IGXColumnDefinition = this.getColumnDefinition(colIndex);

            if (def.editMode !== CELL_EDIT_CLOSED_DROPDOWN && def.editMode !== CELL_EDIT_OPEN_DROPDOWN) {
                this.massUpdateValue(colIndex, rowIndex, event.newValue.content);
            }

            this.notify();
        }
    }

    public onCheckboxChanged(event: any, boundedValue: any, cell: IgxGridCellComponent): void {
        this._data[this.getRowIndex(cell)]['selected'] = !boundedValue;
    }

    public onDropDownSelection(event: any, boundedValue: any, cell: IgxGridCellComponent): void {
        if (event !== undefined && boundedValue !== undefined) {
            const def = this.getColumnDefinition(this.getColumnIndex(cell));
            this._data[this.getRowIndex(cell)][def.field].content = event.newSelection.value;
            this.massUpdateValue(this.getColumnIndex(cell), this.getRowIndex(cell), event.newSelection.value);
            this.updateAffectedColumns(def, this.getRowIndex(cell), event.newSelection.value);
            this._grid.endEdit(true);

            this.notify();
        }
    }
}
