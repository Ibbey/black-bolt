import { Observable } from 'rxjs';

import { IConfigurationEntry } from '@models/configuration/_configuration.barrel';
export interface IConfigurationService {

    getConfiguration(userId: string, rowId: number): Observable<IConfigurationEntry>;

    saveConfiguration(userId: string, rowId: number, config: any): Observable<any>;
}
