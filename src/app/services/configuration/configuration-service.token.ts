import { InjectionToken } from '@angular/core';

import { IConfigurationService } from './configuration-service.contract';

export const ConfigurationServiceToken = new InjectionToken<IConfigurationService>('ConfigurationService');
