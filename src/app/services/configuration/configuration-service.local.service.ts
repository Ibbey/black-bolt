import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { IConfigurationService } from './configuration-service.contract';

import { IConfigurationEntry } from '@models/configuration/_configuration.barrel';

export class LocalConfigurationService implements IConfigurationService {
    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;

    // Constructor ********************************************************************************
    constructor (http: HttpClient) {
        this._http = http;
    }

    // Interface Methods **************************************************************************
    public getConfiguration(userId: string, rowId: number): Observable<IConfigurationEntry> {
        const path = '../../../assets/data/mock-configuration.json';

        return this._http.get(path).pipe(
            map((response: any) => {
                return <IConfigurationEntry>(response);
            }),
            catchError(error => {
                console.log(error);
                return of(undefined);
            })
        );
    }

    public saveConfiguration(userId: string, rowId: number, config: any): Observable<any> {
        return undefined;
    }
}
