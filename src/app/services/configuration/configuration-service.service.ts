import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IConfigurationService } from './configuration-service.contract';

export class ConfigurationService implements IConfigurationService {
    // Constants **********************************************************************************
    private static readonly getConfigUri = '/configuration/id=';
    private static readonly saveConfigUri = '/configuration/';

    // Member Variables ***************************************************************************
    private readonly _http: HttpClient;
    private readonly _baseUri: string;

    // Constructor ********************************************************************************
    constructor (http: HttpClient, baseUri: string) {
        this._http = http;
        this._baseUri = baseUri;
    }

    // Interface Methods **************************************************************************
    public getConfiguration(userId: string, rowId: number): Observable<any> {
        return undefined;
    }

    public saveConfiguration(userId: string, rowId: number, config: any): Observable<any> {
        return undefined;
    }
}
