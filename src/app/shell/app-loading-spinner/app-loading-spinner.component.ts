import { Component, isDevMode, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';

import { GlobalState } from '@store/state/global.state';

import * as ACTIONS from '@store/actions/view.actions';

const ESCAPE_KEY_LONG = 'Escape';
const ESCAPE_KEY_SHORT = 'Esc';

@Component({
    selector: 'app-loading-spinner',
    templateUrl: './app-loading-spinner.component.html',
    styleUrls: [ './app-loading-spinner.component.scss' ]
})
export class BlackBoltAppLoadingSpinnerComponent {
    // Member Variables ***************************************************************************
    private readonly _store: Store<GlobalState>;

    // Constructor ********************************************************************************
    constructor (store: Store<GlobalState>) {
        this._store = store;
    }

    // Event Handlers *****************************************************************************
    @HostListener('document:keyup', ['$event'])
    private handleDocumentKeyUp(event: KeyboardEvent): void {
        if (isDevMode() === true) {
            switch (event.key) {
                case ESCAPE_KEY_LONG:
                case ESCAPE_KEY_SHORT:
                    this._store.dispatch(new ACTIONS.DismissLoadingStateAction());
                    break;
                default:
                    break;
            }
        }
    }
}
