import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { IConfigurationOption } from '@models/configuration/_configuration.barrel';

import { GlobalState } from '@store/state/global.state';
import { getIsLoading } from '@store/selectors/view.selector';
import * as VIEW_ACTIONS from '@store/actions/view.actions';

const TEST_USER = 'test_user';
const TEST_ROW = 123;

@Component({
    selector: 'app-shell',
    templateUrl: './shell.component.html',
    styleUrls: [ './shell.component.scss' ]
})
export class BlackBoltShellComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<GlobalState>;
    private readonly _activatedRoute: ActivatedRoute;

    private _isLoading$: Observable<boolean>;
    private _isLoading: boolean;

    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get isLoading(): boolean {
        return this._isLoading;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<GlobalState>, activatedRoute: ActivatedRoute) {
        this._store = store;
        this._activatedRoute = activatedRoute;

        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.parseRoutes();
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._isLoading$ = this._store.pipe(select(getIsLoading));
    }

    private parseRoutes(): void {
        this._subscriptions.push(this._activatedRoute.queryParams.subscribe((params: Params) => {
            let userId = params['userId'];
            let rowId = params['rowId'];

            if (userId === undefined && rowId === undefined) {
                userId = TEST_USER;
                rowId = TEST_ROW;
            }

            this._store.dispatch(new VIEW_ACTIONS.SetConfigurationOptionAction(<IConfigurationOption>{ userId: userId, rowId: rowId }));
        }));
    }

    private subscribe(): void {
        if (this._isLoading$ !== undefined) {
            this._subscriptions.push(this._isLoading$.subscribe(x => this._isLoading = x));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
            this._subscriptions = undefined;
        }
    }
}
