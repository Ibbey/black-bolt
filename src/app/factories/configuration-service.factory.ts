import { HttpClient } from '@angular/common/http';

import { environment } from '@env/environment';

import { IConfigurationService } from '@services/configuration/configuration-service.contract';
import { ConfigurationService } from '@services/configuration/configuration-service.service';
import { LocalConfigurationService } from '@services/configuration/configuration-service.local.service';

export function configurationServiceFactory(http: HttpClient): IConfigurationService {

    const uri = environment.configurationUri;

    if (uri === 'local') {
        return new LocalConfigurationService(http);
    }

    return new ConfigurationService(http, uri);
}
