import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';


import { GlobalState } from '../state/global.state';

import * as ACTIONS from '../actions/view.actions';

@Injectable()
export class ViewEffects {
    // Member Variables ***************************************************************************
    private readonly _actions$: Actions;
    private readonly _store: Store<GlobalState>;

    // Effects ************************************************************************************

    // Constructor ********************************************************************************
    constructor (actions$: Actions, store: Store<GlobalState>) {
        this._actions$ = actions$;
        this._store = store;

        this.initialize();
    }

    // Private Methods *****************************************************************************
    private initialize(): void {

    }
}
