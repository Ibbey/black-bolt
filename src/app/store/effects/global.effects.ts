import { ConfigurationEffects } from './configuration.effects';
import { ViewEffects } from './view.effects';

export const globalEffects = [
    ConfigurationEffects,
    ViewEffects
];
