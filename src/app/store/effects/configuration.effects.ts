import { Injectable, Inject } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';

import { IConfigurationService } from '@services/configuration/configuration-service.contract';
import { ConfigurationServiceToken } from '@services/configuration/configuration-service.token';

import { GlobalState } from '../state/global.state';

import { getConfigOption } from '../selectors/view.selector';
import { getConfigEntry, getConfigModification } from '../selectors/configuration.selector';

import * as CONFIG_ACTIONS from '../actions/configuration.actions';
import * as VIEW_ACTIONS from '../actions/view.actions';

@Injectable()
export class ConfigurationEffects {
    // Member Variables ***************************************************************************
    private _actions$: Actions;
    private _store: Store<GlobalState>;
    private _service: IConfigurationService;

    // Effects ************************************************************************************
    @Effect() configurationEntryEffect$: Observable<Action>;
    @Effect() cancelConfigrationEffect$: Observable<Action>;
    @Effect() saveConfigurationEffect$: Observable<Action>;

    // Constructor ********************************************************************************
    constructor (actions$: Actions, store: Store<GlobalState>, @Inject(ConfigurationServiceToken) service: IConfigurationService) {
        this._actions$ = actions$;
        this._store = store;
        this._service = service;

        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this.configurationEntryEffect$ = this.getConfigurationEntryEffect();
        this.cancelConfigrationEffect$ = this.getCancelConfigurationEffect();
        this.saveConfigurationEffect$ = this.getSaveConfigurationEffect();
    }

    private getConfigurationEntryEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(VIEW_ACTIONS.SET_CONFIG_OPTION),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const payload = (action as VIEW_ACTIONS.SetConfigurationOptionAction).payload;
                return this._service.getConfiguration(payload.userId, payload.rowId).pipe(
                    switchMap((response) => {
                        return of(new CONFIG_ACTIONS.SetConfigurationEntryAction(response));
                    })
                );
            })
        );
    }

    private getCancelConfigurationEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(CONFIG_ACTIONS.CANCEL_CONFIG_MODIFICATION),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const entry = getConfigEntry(state);
                return from([
                    new CONFIG_ACTIONS.ClearConfigurationEntryAction(),
                    new CONFIG_ACTIONS.SetConfigurationEntryAction(entry)
                ]);
            })
        );
    }

    private getSaveConfigurationEffect(): Observable<Action> {
        return this._actions$.pipe(
            ofType(CONFIG_ACTIONS.SAVE_CONFIG_MODIFICATION),
            withLatestFrom(this._store),
            switchMap(([action, state]) => {
                const modification = getConfigModification(state);
                const option = getConfigOption(state);
                return this._service.saveConfiguration(option.userId, option.rowId, modification).pipe(
                    switchMap((response) => {
                        return of(new CONFIG_ACTIONS.SaveConfigurationModificationSuccessAction());
                    })
                );
            })
        );
    }
}
