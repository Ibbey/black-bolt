import { ConfigurationState } from './configuration.state';
import { ViewState } from './view.state';

export interface GlobalState {
    configuration: ConfigurationState;
    view: ViewState;
}
