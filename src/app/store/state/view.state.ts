import { IConfigurationOption } from '@models/configuration/_configuration.barrel';

export interface ViewState {
    isLoading: boolean;
    configOption: IConfigurationOption;
}
