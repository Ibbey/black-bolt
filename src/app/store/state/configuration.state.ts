import { IConfigurationEntry, IConfigurationModification } from '@models/configuration/_configuration.barrel';

export interface ConfigurationState {
    entry: IConfigurationEntry;
    modified: IConfigurationModification;
}
