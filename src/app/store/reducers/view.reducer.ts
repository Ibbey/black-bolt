import { ViewState } from '../state/view.state';

import * as ACTIONS from '../actions/view.actions';


export const INITIAL_STATE: ViewState = {
    isLoading: false,
    configOption: undefined
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): ViewState {
    switch (action.type) {
        case ACTIONS.SET_CONFIG_OPTION:
            return Object.assign({}, state, {
                configOption: (action as ACTIONS.SetConfigurationOptionAction).payload
            });
        case ACTIONS.DISMISS_LOADING_STATE:
            return Object.assign({}, state, {
                isLoading: false
            });
        default:
            return state;
    }
}
