import { InjectionToken } from '@angular/core';
import { ActionReducerMap, MetaReducer, Action } from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '@env/environment';
import { ngrxLogger } from '@common/logging/ngrx-logger';

import { GlobalState } from '../state/global.state';

import * as fromConfig from './configuration.reducer';
import * as fromView from './view.reducer';

export const globalReducers: ActionReducerMap<GlobalState> = {
    configuration: fromConfig.reducer,
    view: fromView.reducer
};

export const metaReducers: MetaReducer<GlobalState>[] = !environment.production ? [storeFreeze, ngrxLogger ] : [];

export const GlobalReducersToken = new InjectionToken<ActionReducerMap<GlobalState, Action>>('GlobalReducersToken');
