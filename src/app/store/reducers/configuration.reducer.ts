import { ConfigurationState } from '../state/configuration.state';

import * as ACTIONS from '../actions/configuration.actions';

export const INITIAL_STATE: ConfigurationState = {
    entry: undefined,
    modified: undefined
};

export function reducer(state = INITIAL_STATE, action: ACTIONS.All): ConfigurationState {
    switch (action.type) {
        case ACTIONS.SET_CONFIG_ENTRY:
            return Object.assign({}, state, {
                entry: (action as ACTIONS.SetConfigurationEntryAction).payload
            });
        case ACTIONS.SET_CONFIG_MODIFICATION:
            return Object.assign({}, state, {
                modified: (action as ACTIONS.SetConfigurationModificationAction).payload
            });
        case ACTIONS.CANCEL_CONFIG_MODIFICATION:
            return Object.assign({}, state, {
                modified: undefined
            });
        case ACTIONS.CLEAR_CONFIG_ENTRY:
            return Object.assign({}, state, {
                entry: undefined
            });
        default:
            return state;
    }
}
