import { Action } from '@ngrx/store';

import { IConfigurationEntry, IConfigurationModification } from '@models/configuration/_configuration.barrel';

export const SET_CONFIG_ENTRY = '[CONFIG] set the configuration entry retrieved from the configuration service';
export const SET_CONFIG_MODIFICATION = '[CONFIG] set the configuration modification based on updates from the user interactions';
export const CANCEL_CONFIG_MODIFICATION = '[CONFIG] cancel the configuration modification';
export const SAVE_CONFIG_MODIFICATION = '[CONFIG] save the configuration modification';
export const SAVE_CONFIG_MODIFICATION_SUCCESS = '[CONFIG] the configuration was successfully saved';

export const CLEAR_CONFIG_ENTRY = '[CONFIG] clear the configuration entry';

export class SetConfigurationEntryAction implements Action {
    public readonly type = SET_CONFIG_ENTRY;
    constructor (public readonly payload: IConfigurationEntry) {}
}

export class SetConfigurationModificationAction implements Action {
    public readonly type = SET_CONFIG_MODIFICATION;
    constructor (public readonly payload: IConfigurationModification) {}
}

export class CancelConfigurationModificationAction implements Action {
    public readonly type = CANCEL_CONFIG_MODIFICATION;
}

export class SaveConfigurationModificationAction implements Action {
    public readonly type = SAVE_CONFIG_MODIFICATION;
}

export class SaveConfigurationModificationSuccessAction implements Action {
    public readonly type = SAVE_CONFIG_MODIFICATION_SUCCESS;
}

export class ClearConfigurationEntryAction implements Action {
    public readonly type = CLEAR_CONFIG_ENTRY;
}

export type All =
    SetConfigurationEntryAction |
    SetConfigurationModificationAction |
    CancelConfigurationModificationAction |
    SaveConfigurationModificationAction |
    ClearConfigurationEntryAction;
