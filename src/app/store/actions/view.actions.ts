import { Action } from '@ngrx/store';

import { IConfigurationOption } from '@models/configuration/_configuration.barrel';

export const SET_CONFIG_OPTION = '[VIEW] set the configuration option - acquired from the url params';
export const DISMISS_LOADING_STATE = '[VIEW] force the loading state to clear';

export class SetConfigurationOptionAction implements Action {
    public readonly type = SET_CONFIG_OPTION;
    constructor (public readonly payload: IConfigurationOption) {}
}

export class DismissLoadingStateAction implements Action {
    public readonly type = DISMISS_LOADING_STATE;
}

export type All =
    SetConfigurationOptionAction |
    DismissLoadingStateAction;
