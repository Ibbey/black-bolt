import { createSelector, createFeatureSelector } from '@ngrx/store';

import { GlobalState } from '../state/global.state';
import { ViewState } from '../state/view.state';

const getViewState = createFeatureSelector<GlobalState, ViewState>('view');

export const getIsLoading = createSelector(getViewState, (state: ViewState) => {
    if (state !== undefined) {
        return state.isLoading;
    }

    return undefined;
});

export const getConfigOption = createSelector(getViewState, (state: ViewState) => {
    if (state !== undefined) {
        return state.configOption;
    }

    return undefined;
});
