import { createSelector, createFeatureSelector } from '@ngrx/store';

import { GlobalState } from '../state/global.state';
import { ConfigurationState } from '../state/configuration.state';

const getConfigState = createFeatureSelector<GlobalState, ConfigurationState>('configuration');

export const getConfigEntry = createSelector(getConfigState, (state: ConfigurationState) => {
    if (state !== undefined) {
        return state.entry;
    }

    return undefined;
});

export const getConfigModification = createSelector(getConfigState, (state: ConfigurationState) => {
    if (state !== undefined) {
        return state.modified;
    }
});
