import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlackBoltShellComponent } from './shell/shell.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', component: BlackBoltShellComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
