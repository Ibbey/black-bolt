import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { BlackBoltEditActionsComponent } from './edit-actions.component';
import { AppModule } from '../../app.module';

describe('BlackBoltEditActionsComponent', () => {
    let component: BlackBoltEditActionsComponent;
    let fixture: ComponentFixture<BlackBoltEditActionsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BlackBoltEditActionsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
