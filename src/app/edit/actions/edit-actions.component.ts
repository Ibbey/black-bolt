import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import { GlobalState } from '@store/state/global.state';

import * as ACTIONS from '@store/actions/configuration.actions';

@Component({
    selector: 'app-edit-actions',
    templateUrl: './edit-actions.component.html',
    styleUrls: [ './edit-actions.component.scss' ]
})
export class BlackBoltEditActionsComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<GlobalState>;

    // Public Properties **************************************************************************

    // Constructor ********************************************************************************
    constructor (store: Store<GlobalState>) {
        this._store = store;
    }

    // Private Methods ****************************************************************************

    // Interface Methods **************************************************************************
    public ngOnInit(): void {

    }

    public ngOnDestroy(): void {

    }

    // Event Handlers *****************************************************************************
    public handleCancelClicked(event: MouseEvent): void {
        this._store.dispatch(new ACTIONS.CancelConfigurationModificationAction());
    }

    public handleSaveClicked(event: MouseEvent): void {
        this._store.dispatch(new ACTIONS.SaveConfigurationModificationAction());
    }
}
