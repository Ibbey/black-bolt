import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { BlackBoltEditToolbarComponent } from './edit-toolbar.component';
import { AppModule } from '../../app.module';

describe('BlackBoltEditToolbarComponent', () => {
    let component: BlackBoltEditToolbarComponent;
    let fixture: ComponentFixture<BlackBoltEditToolbarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BlackBoltEditToolbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
