import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { BlackBoltGridComponent } from '../../grid/ipx-grid.component';
import { IConfigurationEntry, IConfigurationModification } from '@models/configuration/_configuration.barrel';

import { GlobalState } from '@store/state/global.state';
import { getConfigEntry } from '@store/selectors/configuration.selector';

import * as ACTIONS from '@store/actions/configuration.actions';

@Component({
    selector: 'app-edit-content',
    templateUrl: './edit-content.component.html',
    styleUrls: [ './edit-content.component.scss' ]
})
export class BlackBoltEditContentComponent implements OnInit, OnDestroy {
    // Member Variables ***************************************************************************
    private readonly _store: Store<GlobalState>;

    private _entry$: Observable<IConfigurationEntry>;
    private _entry: IConfigurationEntry;

    @ViewChild('grid', { read: BlackBoltGridComponent }) private _grid: BlackBoltGridComponent;
    private _subscriptions: Subscription[];

    // Public Properties **************************************************************************
    public get entry(): IConfigurationEntry {
        return this._entry;
    }

    public set entry(value: IConfigurationEntry) {
        this._entry = value;
    }

    // Constructor ********************************************************************************
    constructor (store: Store<GlobalState>) {
        this._store = store;
        this.initialize();
    }

    // Interface Methods **************************************************************************
    public ngOnInit(): void {
        this.subscribe();
    }

    public ngOnDestroy(): void {
        this.unsubscribe();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._subscriptions = [];

        this._entry$ = this._store.pipe(select(getConfigEntry));
    }

    private clear(): void {
        this.entry = undefined;
    }

    private subscribe(): void {
        if (this._entry$ !== undefined) {
            this._subscriptions.push(this._entry$.subscribe(this.handleConfigEntryChanged.bind(this)));
        }
    }

    private unsubscribe(): void {
        if (this._subscriptions !== undefined) {
            this._subscriptions.forEach(x => x.unsubscribe());
        }
    }

    // Event Handlers *****************************************************************************
    public handleOnConfigurationModified(modification: IConfigurationModification): void {
        this._store.dispatch(new ACTIONS.SetConfigurationModificationAction(modification));
    }

    private handleConfigEntryChanged(entry: IConfigurationEntry): void {
        this.clear();
        this.entry = entry;
        this._grid.refresh(entry);
    }
}
