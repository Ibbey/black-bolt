import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';

import { BlackBoltEditContentComponent } from './edit-content.component';
import { AppModule } from '../../app.module';

describe('BlackBoltEditContentComponent', () => {
    let component: BlackBoltEditContentComponent;
    let fixture: ComponentFixture<BlackBoltEditContentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule
            ],
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BlackBoltEditContentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
