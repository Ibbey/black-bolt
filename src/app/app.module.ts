// Angular Modules ********************************************************************************
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// @ngrx Modules **********************************************************************************
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// Angular Material Modules ***********************************************************************
import { MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatIconModule,
         MatRadioModule, MatSidenavModule, MatTabsModule, MatToolbarModule, MatDialogModule,
         MatSlideToggleModule, MatSelectModule, MatProgressBarModule, MatProgressSpinnerModule,
         MatFormFieldModule, MatAutocompleteModule, MatInputModule, MatTooltipModule,
         MatExpansionModule, MatSnackBarModule, MatOptionModule, MatTableModule, MatDatepickerModule,
         MatSnackBar, RippleGlobalOptions, MatNativeDateModule, MAT_DIALOG_DATA, MAT_SNACK_BAR_DATA,
         MAT_RIPPLE_GLOBAL_OPTIONS } from '@angular/material';

// Infragistics Ignite UI Components **************************************************************
import { IgxAvatarModule, IgxBadgeModule, IgxButtonModule, IgxGridModule, IgxIconModule,
         IgxInputGroupModule, IgxProgressBarModule, IgxRippleModule, IgxSwitchModule,
         IgxComboModule, IgxDropDownModule, IgxTooltipModule } from 'igniteui-angular';

// Internal Permutr Modules ***********************************************************************
import { AppRoutingModule } from './app-routing.module';

// Black-Bolt NGRX Reducers ***********************************************************************
import { globalReducers, metaReducers, GlobalReducersToken } from './store/reducers/global.reducer';

// Black-Bolt NGRX Effects ************************************************************************
import { globalEffects } from './store/effects/global.effects';

// Black-Bolt Services ****************************************************************************
import { ConfigurationServiceToken } from './services/configuration/configuration-service.token';
import { configurationServiceFactory } from './factories/configuration-service.factory';

// Black-Bolt Components **************************************************************************
import { BlackBoltBootstrapComponent } from './bootstrap/bootstrap.component';
import { BlackBoltShellComponent } from './shell/shell.component';
import { BlackBoltAppLoadingSpinnerComponent } from './shell/app-loading-spinner/app-loading-spinner.component';
import { BlackBoltEditActionsComponent } from './edit/actions/edit-actions.component';
import { BlackBoltEditContainerComponent } from './edit/container/edit-container.component';
import { BlackBoltEditContentComponent } from './edit/content/edit-content.component';
import { BlackBoltEditToolbarComponent } from './edit/toolbar/edit-toolbar.component';
import { BlackBoltGridComponent } from './grid/ipx-grid.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    IgxAvatarModule,
    IgxBadgeModule,
    IgxButtonModule,
    IgxGridModule,
    IgxIconModule,
    IgxInputGroupModule,
    IgxProgressBarModule,
    IgxRippleModule,
    IgxSwitchModule,
    IgxComboModule,
    IgxDropDownModule,
    IgxTooltipModule,
    StoreModule.forRoot(GlobalReducersToken, { metaReducers }),
    EffectsModule.forRoot(globalEffects),
    StoreDevtoolsModule.instrument({
      maxAge: 75
    }),
    AppRoutingModule
  ],
  declarations: [
    BlackBoltBootstrapComponent,
    BlackBoltShellComponent,
    BlackBoltAppLoadingSpinnerComponent,
    BlackBoltEditActionsComponent,
    BlackBoltEditContainerComponent,
    BlackBoltEditContentComponent,
    BlackBoltEditToolbarComponent,
    BlackBoltGridComponent
  ],
  entryComponents: [

  ],
  providers: [
    {
      provide: GlobalReducersToken,
      useValue: globalReducers
    },
    {
      provide: ConfigurationServiceToken,
      useFactory: configurationServiceFactory,
      deps: [ HttpClient ]
    }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [
    BlackBoltBootstrapComponent
  ]
})
export class AppModule {
}
