import { IConfigurationAffectedColumn } from './configuration-affected-column';

export interface IConfigurationDropDownMapping {
    columnKey: string;
    value: any;
    affectedColumns: IConfigurationAffectedColumn[];
}
