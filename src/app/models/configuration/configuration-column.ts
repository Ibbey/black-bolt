export interface IConfigurationColumn {
    key: string;
    displayName: string;
    dataType: string;
    editType: string;
    editable?: boolean;
    massUpdate?: boolean;
    options?: any[];
}
