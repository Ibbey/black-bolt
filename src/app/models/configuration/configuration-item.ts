import { IConfigurationDropDownMapping, IConfigurationError } from './_configuration.barrel';

export interface IConfigurationItem {
    values: any[];
    dropDownMapping: IConfigurationDropDownMapping[];
    errors: IConfigurationError[];
}
