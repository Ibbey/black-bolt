export interface IConfigurationError {
    columnKey: string;
    level: string;
    message: string;
}
