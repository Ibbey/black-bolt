export interface IConfigurationAffectedColumn {
    columnKey: string;
    value: any;
    options: any[];
}
