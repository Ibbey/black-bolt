import { IConfigurationColumn, IConfigurationItem } from './_configuration.barrel';

export interface IConfigurationEntry {
    columns: IConfigurationColumn[];
    items: IConfigurationItem[];
}
