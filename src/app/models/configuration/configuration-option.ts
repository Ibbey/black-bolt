export interface IConfigurationOption {
    userId: string;
    rowId: number;
}
