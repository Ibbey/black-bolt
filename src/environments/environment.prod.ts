import { IEnvironmentMetaData } from './environment.contract';

export const environment: IEnvironmentMetaData = {
    configurationUri: 'local',
    version: '1.0.0',
    production: true
};
