export interface IEnvironmentMetaData {
    configurationUri: string;
    version: string;
    production: boolean;
}
